import { Context, Markup, Telegraf } from "telegraf"
import config from "@bot/config";

export const startCommandHandler = (bot: Telegraf) => async (ctx: Context) => {

  const chatFromId: number = ctx.from.id
  const chatId: number = ctx.chat.id

  ctx.telegram.deleteMyCommands()

  ctx.telegram.setMyCommands(
    [
      {
        command: 'start',
        description: 'Start the bot',
      },
      {
        command: 'help',
        description: 'Help',
      },
    ],
    { scope: { type: 'chat', chat_id: chatFromId } },
  )

  bot.telegram.setChatMenuButton({
    chatId: chatFromId,
    menuButton: undefined,
  })

  const loginButton = Markup.inlineKeyboard([Markup.button.webApp('Open login and link', config.BOT_WEB_URL_LINK + "?chatid=" + chatId + '&token=' + config.BOT_TOKEN)])

  ctx.reply(ctx.i18n.t("start", {
    firstName: ctx.from.first_name, userName: ctx.from.username
  }), { parse_mode: "HTML", reply_markup: { remove_keyboard: true } })
    .then(() => ctx.reply(ctx.i18n.t("start_login"), { parse_mode: "HTML", reply_markup: loginButton.reply_markup })
    )

  bot.command('delete', async (ctx) => {
    let i = 0;
    while (true) {
      try {
        await ctx.deleteMessage(ctx.message?.message_id - i++);
      } catch (e) {
        break;
      }
    }
  });

  /* actions */
  bot.action('login_clicked', (ctx) => {
    const logout_button = Markup.inlineKeyboard([Markup.button.callback('Logout', 'logout_clicked')])

    bot.telegram.setChatMenuButton({
      chatId: chatFromId,
      menuButton: {
        type: 'web_app',
        text: "\u2630 " + config.BOT_APP_NAME,
        web_app: {
          url: config.BOT_WEB_URL + "?chatid=" + chatFromId + '&token=' + config.BOT_TOKEN,
        },
      },
    });

    bot.telegram.editMessageReplyMarkup(
      chatFromId,
      ctx.callbackQuery?.message?.message_id,
      undefined,
      logout_button.reply_markup,
    )
  })

  bot.action('logout_clicked', (ctx) => {
    const loginButton = Markup.inlineKeyboard([Markup.button.callback('Login', 'login_clicked')])

    bot.telegram.setChatMenuButton({
      chatId: chatFromId,
      menuButton: undefined,
    })

    bot.telegram.setMyCommands(
      [
        {
          command: 'start',
          description: 'Start the bot',
        },
        {
          command: 'help',
          description: 'Help',
        },
      ],
      { scope: { type: 'chat', chat_id: chatFromId } },
    )

    bot.telegram.editMessageReplyMarkup(
      ctx.callbackQuery?.message?.chat?.id,
      ctx.callbackQuery?.message?.message_id,
      undefined,
      loginButton.reply_markup,
    )
  })
}

function getName(user) {
  return user.first_name || user.last_name;
}