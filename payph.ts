import { Bot, Keyboard, InlineKeyboard } from "grammy"
import { Menu } from "@grammyjs/menu"

const bot = new Bot("6477047176:AAEKx5ToZXKcfH2RuL9RABAx5QKehdH2urA")

await bot.api.setMyCommands([
  { command: "start", description: "Start the bot" },
  { command: "help", description: "Show help text" },
  { command: "settings", description: "Open settings" },
])

const menu = new Menu("my-menu-identifier")
  .text("A", (ctx) => ctx.reply("You pressed A!")).row()
  .text("B", (ctx) => ctx.reply("You pressed B!"))


const keyboard = new InlineKeyboard().webApp('Open PayPH',`https://payph.spotonapp.com`);

bot.command("start", async (ctx) => {

  // remove's the keyboard (menu), if this has been defined
  // await ctx.reply("Menu",   { reply_markup: { remove_keyboard: true } })

  /* const keyboards = new Keyboard().text(">").row().text("x").row().text("<");
    await ctx.reply("Menu",  {reply_markup: keyboards });  */

  ctx.reply("<strong>Hi " + ctx.msg.chat.id + "!</strong> <i>Welcome</i> to <strong>PayPH</strong>.", { parse_mode: "HTML", reply_markup: keyboard });
  /* await bot.api.chat.sendMessage(
    ctx.msg.chat.id,
    "<strong>Hi " + ctx.msg.chat.first_name + "!</strong> <i>Welcome</i> to <strong>PayPH</strong>.",
    { parse_mode: "HTML" },
  ) */
})



// telegraf

/* await ctx.reply("Test", Markup.keyboard([
      ["🦍 Become a Hunter", "🔥 TOP Hunters"],
      ["📢 Personal Info", "📞 Feedback"],
      ["🤳 Message the Aim", "👁 Message the Hunter"],
      ["👹 Catch the Aim"],
    ])
      .resize()) */

// await ctx.reply("Link account", Markup.inlineKeyboard([[Markup.button.url("Become a Hunter", "https://test.com")]]))